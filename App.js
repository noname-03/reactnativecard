import React from 'react';
import {Image, Text, View, StyleSheet } from 'react-native';

const App = () =>{
  return(
    <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <View style={{ backgroundColor: "#eee", borderRadius: 10, overflow: "hidden" }}>
          <View>
          <Image
          style= {styles.image}
          source={{uri: 'https://res.cloudinary.com/djyjm9ayd/image/upload/v1643249117/19025216_1439860629412992_3671167199250762358_o_hzryz8.png',}}/>
          </View>
          <View style={{ padding: 10}}>
            <Text style={{alignSelf: 'center', fontWeight: 'bold', color: 'black' }}>Styling Di React Native</Text>
            <Text style={{ color: "#777", paddingTop: 5 , alignSelf: 'center' }}>
              React Native - Binar Academy
            </Text>
            <Text style={{ color: "black", paddingTop: 5 , alignSelf: 'center' }}>
              Description of the image Description of the image Description of the image Description of the image Description of the image Description of the image Description of the image 
            </Text>
          </View>
        </View>
    </View>
  )
}

const styles = StyleSheet.create({
  image: {
    height: 360,
    width: 360
  },
});

export default App;